// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:feng_shui_number_assignment/helpers.dart';
import 'dart:async';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

Future<void> main() async {
  setUpAll(() {
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  List<Map<String, dynamic>> testCases = [];
  List<String> taboos = [];

  var jsonText = await rootBundle.loadString('assets/testcases.json');
  List<Map<String, dynamic>> data = json.decode(jsonText);
  testCases = data;

  var tabooText = await rootBundle.loadString('assets/taboos.json');
  List<String> datum = json.decode(tabooText);
  taboos = datum;

  test('Check taboos numbers', () {
    final helper = Helpers();
    List<bool> results =
        testCases.map((e) => (e['taboo'] == 'true' ? true : false)).toList();
    List<String> phonenums =
        testCases.map((e) => e['phonenum'].toString()).toList();

    for (int i = 0; i < phonenums.length; i++) {
      expect(helper.checkIfIsTaboo(phonenums[i], taboos), results[i]);
    }
  });
}
