import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:feng_shui_number_assignment/helpers.dart';
import 'dart:async';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Feng Shui Numbers'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _textFieldController = TextEditingController();
  String _value = "";
  bool _isTaboo = false;
  bool _isGoodFengshuiNum = false;
  String _network = "";
  List<String> tabooList = [];
  Helpers helper = Helpers();

  Future<String> loadJsonData() async {
    var jsonText = await rootBundle.loadString('assets/taboos.json');
    List<dynamic> data = json.decode(jsonText);
    setState(() => tabooList = data.map((e) => e.toString()).toList());
    print(this.tabooList);
    return 'success';
  }

  @override
  void initState() {
    super.initState();
    this._value = "";
    this._network = "";
    this._isTaboo = false;
    this._isGoodFengshuiNum = false;
    this.loadJsonData();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle badFengShuiTextStyle = new TextStyle(
      color: Colors.redAccent,
      fontSize: Theme.of(context).textTheme.headline6?.fontSize,
      fontWeight: FontWeight.w700,
    );
    TextStyle goodFengShuiTextStyle = new TextStyle(
      color: Colors.green,
      fontSize: Theme.of(context).textTheme.headline6?.fontSize,
      fontWeight: FontWeight.w700,
    );
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 200,
              child: (this._network != "")
                  ? Image.asset(
                      "assets/${helper.checkIfIsInAvailableMobileNetworks(this._value)}.jpg")
                  : Container(),
            ),
            TextField(
              onChanged: (value) {
                setState(() {
                  this._value = value;
                  this._network =
                      helper.checkIfIsInAvailableMobileNetworks(this._value);
                  this._isTaboo =
                      (helper.checkIfIsTaboo(this._value, this.tabooList));
                  this._isGoodFengshuiNum =
                      helper.checkIfIsGoodFengShui(this._value);
                });
              },
              controller: _textFieldController,
              decoration: InputDecoration(hintText: "Phone number"),
            ),
            Text(helper.checkIfNumberCorrect(this._value)
                ? "Your number is correct."
                : "Wrong phone number"),
            if (this._isTaboo) ...[
              Text(
                "Bad Feng Shui Number",
                style: badFengShuiTextStyle,
              )
            ],
            if (this._isGoodFengshuiNum) ...[
              Text(
                "Good Feng Shui Number",
                style: goodFengShuiTextStyle,
              )
            ],
          ],
        ),
      ),
    );
  }
}
