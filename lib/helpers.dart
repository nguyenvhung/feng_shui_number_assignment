import 'package:flutter/material.dart';

class Helpers {
  Map<String, List<String>> mobileNetworks = {
    "viettel": ["086", "096", "097"],
    "mobifone": ["089", "090", "093"],
    "vinaphone": ["088", "091", "094"],
  };
  List<int> nicePairs = [19, 24, 26, 37, 34];

  bool checkIfNumberCorrect(String phoneNumber) {
    int phoneNumAsInt = 0;
    try {
      phoneNumAsInt = int.parse(phoneNumber);
    } catch (e) {
      return false;
    }

    if (phoneNumAsInt.toString().length > 10) {
      return false;
    }

    if (checkIfIsInAvailableMobileNetworks(phoneNumber) == "") {
      return false;
    }

    return true;
  }

  bool checkIfIsTaboo(String phoneNumber, List<String> taboos) {
    if (phoneNumber.length < 2) return false;
    if (taboos.contains(phoneNumber.substring(phoneNumber.length - 2))) {
      return true;
    }
    return false;
  }

  bool checkIfIsGoodFengShui(String phoneNumber) {
    if (phoneNumber.length < 5) return false;
    String first5nums = phoneNumber.substring(0, 5);
    String last5nums = phoneNumber.substring(phoneNumber.length - 5);

    int sumFirst5nums = first5nums
        .split("")
        .map((each) => int.parse(each))
        .toList()
        .reduce((a, b) => a + b);
    int sumLast5nums = last5nums
        .split("")
        .map((each) => int.parse(each))
        .toList()
        .reduce((a, b) => a + b);

    if ((sumFirst5nums == 24 && sumLast5nums == 29) ||
        (sumFirst5nums == 24 && sumLast5nums == 28)) {
    } else {
      return false;
    }
    if (nicePairs.contains(phoneNumber.substring(phoneNumber.length - 2))) {
      return true;
    }
    return false;
  }

  String checkIfIsInAvailableMobileNetworks(String phoneNumber) {
    String toReturn = "";
    if (phoneNumber.length < 3) return toReturn;
    for (String eachNetwork in mobileNetworks.keys) {
      if (mobileNetworks[eachNetwork]?.contains(phoneNumber.substring(0, 3)) ==
          true) {
        return eachNetwork;
      } else {
        toReturn = "";
      }
    }
    return toReturn;
  }
}
